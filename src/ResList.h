#ifndef RESLIST_H
#define RESLIST_H

#include <SDL2/SDL.h>
#include <string>
#include <vector>

class Resource {
public:
    Resource();

    bool read(const char* path);
    void free();

    std::string m_path;
    SDL_Surface* m_surface;
};

class ResList {
public:
    std::vector<Resource> m_resources;

    void add(const char*);
    void free();
};

#endif
