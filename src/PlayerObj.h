#ifndef PLAYEROBJ_H
#define PLAYEROBJ_H

#include "Map.h"

struct PlayerObj {
    float m_x, m_y, m_z;
    float m_sinv, m_cosv;
    float m_angle;
    int m_sectorId;

    void rotate(const float newAngle);
    void move(Map::Map* map, const float stepX, const float stepY);
};

#endif
