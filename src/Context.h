#ifndef CONTEXT_H
#define CONTEXT_H

#include "Img.h"
#include "Map.h"
#include "PlayerObj.h"
#include "ResList.h"
#include "Frustum.h"
#include "EventHandler.h"

enum ModeType {
    MODE_NONE,
    MODE_2D,
    MODE_3D,
};

class Context : public EventHandler {
public:
    void init(Map::Map* map, uint32_t* data, const int screenWidth, const int screenHeight);
    void initYArrays();
    void print();
    void free();

    void shrinkYBottom(const int x, const int value);
    void shrinkYTop(const int x, const int value);
    void drawYMask();
    void draw();

    void handleExit() override;
    void handleKeyUp(const char inputSymbol) override;

    Img m_img;

    int m_yMin;
    int m_yMax;
    int m_yLen;
    int* m_yTop;
    int* m_yBottom;

    ModeType m_mode;
    int m_stop;
    bool m_clippingEnabled;

    Frustum m_frustum;

    PlayerObj m_player;
    ResList m_resList;
    Map::Map* m_map;

    Map::Edge* m_currentEdge;
    Map::Sector* m_currentSector;

    bool m_drawYMask;

private:
    void clearSurface();
};

#endif
