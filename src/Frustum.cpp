#include "Frustum.h"

int Frustum::izToX(const float x, const float iz, const int screenWidth) const {
    /* [(sx / near) = (x / z)  ] =>
     * [ sx = (near * x) / z)  ] =>
     * [ sx = (near * x * iz)  ] */
    const float ndc_x = (near * x * iz) / right;
    const float screenX = (1.0f + ndc_x) * 0.5f * (float)screenWidth;

    return (int)screenX;
}

int Frustum::izToY(const float height, const float iz, const int screenHeight) const {
    /* [(sy / near) = (height / z) ] => 
     * [sy = (near * height / z)   ] => 
     * [sy = (near * height * iz)  ] */
    const float ndc_y = (near * height * iz) / top;

    /* Transform y from [-1, 1] to [0, screenHeight]: */
    const float t = (1.0f + ndc_y) * 0.5f;

    /* Swap vertically (top of screen is at y = 0): */
    const float screenY = (1.0f - t) * (float)screenHeight;

    return (int)screenY;
}
