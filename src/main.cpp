#include "SdlContext.h"
#include "MapParser.h"
#include "MapView2d.h"
#include "MapView3d.h"

void begin(Map::Map* map) {
    SdlContext sdlCtx;

    sdlCtx.init();

    if(!sdlCtx.isValid()) {
        sdlCtx.destroy();
        return;
    }

    Context context;
    context.init(map, sdlCtx.m_screen.data, sdlCtx.m_screen.w, sdlCtx.m_screen.h);

    sdlCtx.setEventHandler(&context);

    while(1) {
        if(context.m_stop) {
            break;
        }

        sdlCtx.handleKeyboardEvent();
        context.draw();
        sdlCtx.render();
    }

    context.free();
    sdlCtx.destroy();
}

void printUsage(const char *programName) {
    fprintf(stderr, "Usage: %s <map.txt>\n", programName);
}

int main(int argc, char *argv[]) {
    const char *mapPathDefault = "../maps/map-b.txt";
    const char* mapPath = (argc == 2) ? argv[1] : (char *)mapPathDefault;

    auto map = MapParser::parse(mapPath);

    begin(&map);

    return 0;
}
