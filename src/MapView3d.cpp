#include "MapView3d.h"
#include "MathTools.h"
#include "Surface.h"
#include "RenderTools.h"
#include "DebugTools.h"
#include <queue>

Map::Vertex MapView3d::transformVertexToPlayerSpace(const PlayerObj *player, const Map::Vertex *v) {
    /* (tx, ty) = tv := v + [-playerX, -playerY]: */
    const float tx = v->x - player->m_x;
    const float ty = v->y - player->m_y;

    /* Rotate(v, 0.5 * PI - playerAngle): 
     * [Player direction vector will overlap Y axis] */
    Map::Vertex result;
    result.x = tx * player->m_sinv - ty * player->m_cosv;
    result.y = tx * player->m_cosv + ty * player->m_sinv;

    return result;
}

static uint32_t uint32_scale(uint32_t color, const float scale) {
    const int n = 4;
    uint8_t color8[n];

    memcpy(color8, &color, n);

    for(int i = 0; i < n; ++i) {
        color8[i] = (uint8_t)(scale * (float)color8[i]);
    }

    memcpy(&color, color8, n);

    return color;
}

static float getFloorScaleForY(const int screenHeight, const int y) {
    const float f = ((float)y / (float)screenHeight);
    float s;

    if(f <= 0.5f) {
        s = 1.0f - f / 0.5f;
    } else {
        s = (f - 0.5f) / 0.5f;
    }

    return s;
}

static void drawVerticalSolidLine(Img* img, const Surface* surface, const Fragment* fragment, int x, int y1, int y2) {
    if(y1 >= y2) {
        return;
    }

    const int yEnd = img->m_height - 1;
    y1 = clamp(y1, 0, yEnd);
    y2 = clamp(y2, 0, yEnd);

    uint32_t color = 0x00ff00ff;

    if(fragment) {
        color = fragment->color;

        if(fragment->isHighlighted) {
            color = 0xccccccff;
        }
    }

    /* Highlight left/right edge: */
    if(fragment && surface->m_middle && (surface->m_a.x == x || surface->m_b.x == x)) {
        color = surface->m_edgeColor;
    }

    const int32_t yStep = img->m_width;
    const int32_t yMax = (y2 - 1);

    uint32_t* pixels = img->getPixel(x, y1);

    /* Highlight upper edge: */
    *pixels = surface->m_edgeColor;

    /* Move img to y1 + 1: */
    img += yStep;

    for(int y = y1 + 1; y < yMax; ++y) {
        auto dstColor = color;

        if(!fragment) {
            const auto colorScale = getFloorScaleForY(yEnd, y);
            dstColor = uint32_scale(color, colorScale);
        }

        *pixels = dstColor;
        pixels += yStep;
    }

    /* Highlight lower edge: */
    *pixels = surface->m_edgeColor;
}

static bool buildSurfaceFromEdge(
        Surface* surface,
        const Context* context, 
        const SectorRange* currentSectorRange,
        const Map::Sector* sector, 
        const Map::Edge* edge) {
    auto map = context->m_map;
    auto& vertices = map->m_vertices;
    const auto player = &(context->m_player);

    auto edgeVertexA = &vertices[edge->vertexA];
    auto edgeVertexB = &vertices[edge->vertexB];

    auto va = MapView3d::transformVertexToPlayerSpace(player, edgeVertexA);
    auto vb = MapView3d::transformVertexToPlayerSpace(player, edgeVertexB);

    if(va.y <= 0.0f && vb.y <= 0.0f) {
        /* Edge completely behind player's view. Skip to next edge. */
        return true;
    }

    /* Unclipped a: */
    const auto uva = va;

    /* Clip segment to viewing frustum: */
    const auto isClipped = context->m_clippingEnabled ? RenderTools::clipSegment(context, &va, &vb) : true;

    if(!isClipped) {
        return true;
    }

    /* Calc inverted z: */
    const float iz1 = 1.0f / va.y;
    const float iz2 = 1.0f / vb.y;

    const float lt1 = MathTools::getLength(uva.x, uva.y, va.x, va.y);
    const float lt2 = MathTools::getLength(va.x, va.y, vb.x, vb.y);

    const auto screenWidth = context->m_img.m_width;

    /* Calc screen-x coords for each vertex: */
    int sx1 = clamp(context->m_frustum.izToX(va.x, iz1, screenWidth), 0, screenWidth - 1);
    int sx2 = clamp(context->m_frustum.izToX(vb.x, iz2, screenWidth), 0, screenWidth - 1);

    surface->m_length = MathTools::getLength(va.x, va.y, vb.x, vb.y);
    surface->m_va = edgeVertexA;
    surface->m_vb = edgeVertexB;
    surface->m_sector = (Map::Sector *)sector;
    surface->m_isCacheInitialized = 0;
    surface->m_upper = NULL;
    surface->m_middle = NULL;
    surface->m_lower = NULL;
    surface->m_edgeColor = 0x000000;
    surface->m_clipFactor = lt1 / lt2;
    surface->m_xSpan = (float)(sx2 - sx1);
    surface->m_length = lt2;
    surface->m_diz = (iz2 - iz1) / (float)surface->m_xSpan;

    surface->m_a.x = sx1;
    surface->m_a.xClamped = max(sx1, currentSectorRange->m_sx1);
    surface->m_a.iz = iz1;

    surface->m_b.x = sx2;
    surface->m_b.xClamped = min(sx2, currentSectorRange->m_sx2);
    surface->m_b.iz = iz2;

    return false;
}

static void drawSurface(Surface* surface, Context* context, const SectorRange* sectorRange, const Map::Edge* edge) {
    auto img = &context->m_img;
    auto middle = surface->m_middle;
    auto lower = surface->m_lower;
    auto upper = surface->m_upper;

    const bool isPortal = edge->isPortal();

    surface->calcYForFragments(context);

    for(int x = surface->m_a.xClamped; x <= surface->m_b.xClamped; ++x) {
        surface->updateYValues(context, x);

        /* Render ceil: */
        int cy1, cy2;
        {
            cy1 = context->m_yTop[x];
            cy2 = (int)middle->y1;

            drawVerticalSolidLine(img, surface, nullptr, x, cy1, cy2);
        }

        /* Render floor: */
        int fy1, fy2;
        {
            fy1 = (int)middle->y2;
            fy2 = context->m_yBottom[x];

            drawVerticalSolidLine(img, surface, nullptr, x, fy1, fy2);
        }

        if(isPortal) {
            /* Render upper texture: */
            {
                const int uy1 = upper->y1;
                const int uy2 = upper->y2;

                drawVerticalSolidLine(img, surface, upper, x, uy1, uy2);
                context->shrinkYTop(x, max(cy2, uy2));
            }

            /* Render lower texture: */
            {
                const int ly1 = lower->y1;
                const int ly2 = lower->y2;

                drawVerticalSolidLine(img, surface, lower, x, ly1, ly2);
                context->shrinkYBottom(x, min(fy1, ly1));
            }
        } else {
            /* Render non-portal solid wall: */
            const int y1 = middle->y1;
            const int y2 = middle->y2;

            drawVerticalSolidLine(img, surface, middle, x, y1, y2);
        }
    }
}

static void drawSingleFrame(Context* context) {
    const char* msgPrefix = __FUNCTION__;

    auto map = context->m_map;

    context->initYArrays();

    std::queue<SectorRange> sectorQueue;

    {
        auto startSectorId = context->m_player.m_sectorId;
        auto initialSector = &map->m_sectors[startSectorId];

        /* Create sector range for initial sector spanned to full width of screen: */
        SectorRange currentSectorRange(startSectorId, 0, context->m_img.m_width - 1);
        sectorQueue.push(currentSectorRange);
    }

    int renderedSectors[map->m_sectors.size()];

    for(int i = 0; i < map->m_sectors.size(); ++i) {
        renderedSectors[i] = 0;
    }

    Fragment upperFragment, lowerFragment, middleFragment;
    const int queueSize = 32;

    do {

        auto& currentSectorRange = sectorQueue.front();
        const auto currentSectorId = currentSectorRange.m_sectorId;

        r_printf("### queue.size: %d; (sector, x1, x2)=(%d, %d, %d); renderedSectors[sector]=%d\n", 
                (int)sectorQueue.size(), currentSectorId, currentSectorRange.m_sx1, currentSectorRange.m_sx2, renderedSectors[currentSectorId]);

        if(sectorQueue.size() >= queueSize || renderedSectors[currentSectorId] >= queueSize) {
            sectorQueue.pop();
            break;
        }

        auto sector = &map->m_sectors[currentSectorId];
        context->m_currentSector = sector;

        for(int edgeIdx = 0; edgeIdx < sector->edges.size(); ++edgeIdx) {
            auto edge = sector->edges[edgeIdx];
            context->m_currentEdge = &edge;

            Surface surface;

            const auto skipSurface = buildSurfaceFromEdge(&surface, context, &currentSectorRange, sector, &edge);

            if(skipSurface
                || surface.m_a.x >= surface.m_b.x
                || surface.m_b.x < currentSectorRange.m_sx1
                || surface.m_a.x > currentSectorRange.m_sx2) {
                continue;
            }

            if(edge.isPortal()) {
                Map::Sector* neighborSector = &map->m_sectors[edge.neighbor];

                surface.m_upper = upperFragment.set(neighborSector->ceilHeight, sector->ceilHeight, FT_UPPER);
                surface.m_lower = lowerFragment.set(sector->floorHeight, neighborSector->floorHeight, FT_LOWER);

                const auto& sxBegin = surface.m_a.xClamped;
                const auto& sxEnd = surface.m_b.xClamped;

                if(sxEnd >= sxBegin) {
                    SectorRange range(edge.neighbor, sxBegin, sxEnd);
                    sectorQueue.push(range);

                    r_printf("Push %p = ({%d, x1=%d, x2=%d});\n", range, range.m_sectorId, range.m_sx1, range.m_sx2);
                }
            }
            
            surface.m_middle = middleFragment.set(sector->floorHeight, sector->ceilHeight, FT_MIDDLE);

            drawSurface(&surface, context, &currentSectorRange, &edge);
        }

        sectorQueue.pop();
        r_printf("Pop();\n");

        ++renderedSectors[currentSectorId];
    } while(!sectorQueue.empty());
}

void MapView3d::draw(Context* context) {
    drawSingleFrame(context);

    if(context->m_drawYMask) {
        context->drawYMask();
    }
}
