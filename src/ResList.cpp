#include "ResList.h"
#include <SDL2/SDL_image.h>

Resource::Resource() {
    m_surface = nullptr;
}

bool Resource::read(const char* path) {
    SDL_Surface* surface = IMG_Load(path);

    if(surface == NULL) {
        fprintf(stderr, "%s\n", SDL_GetError());
        m_surface = NULL;
        return false;
    }

    m_surface = IMG_Load(path);
    m_path = path;

    return true;
}

void Resource::free() {
    if(m_surface) {
        SDL_FreeSurface(m_surface);
        m_surface = nullptr;
    }
}

void ResList::add(const char* path) {
    Resource resource;

    resource.read(path);

    if(resource.m_surface) {
        printf("resList_add(); Uploaded new surface '%s' (w:%d; h:%d; bytesPerPixel: %d).\n", 
                resource.m_path.c_str(), 
                resource.m_surface->w,
                resource.m_surface->h,
                resource.m_surface->format->BytesPerPixel);

        m_resources.push_back(resource);
    }
}

void ResList::free() {
    for(auto& resource: m_resources) {
        printf("resList_free(); Freeing surface '%s'...\n", resource.m_path.c_str());
        resource.free();
    }

    m_resources.clear();
}
