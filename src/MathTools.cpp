#include "MathTools.h"
#include <cmath>

namespace MathTools
{
    int pointOnSide(float ax, float ay, float bx, float by, float cx, float cy) {
        /* 
         *
         * AB - line
         * C - point
         *
         * B - A: slope of line
         * normal vector: [Y, -X] = [(by - ay), (ax - bx)]
         *
         * <(C - A), N> = (cx - ax) * (by - ay) + (cy - ay) * (ax - bx)
         *
         * AB = (0, 0) -> (1, 1)
         *
         * C(1, 0) (right to AB)
         * (1 - 0) * (1 - 0) + (0 - 0) * ... = 1
         *
         * C(0, 1) (left to AB)
         * (0 - 0) * (1 - 0) + (1 - 0) * (0 - 1) = -1
         *
         * C(0.5, 0.5) (on line AB)
         * (0.5 - 0) * (1.0 - 0.0) + (0.5 - 0.0) * (0.0 - 1.0) = 0
         *
         */
        return (cx - ax) * (by - ay) + (cy - ay) * (ax - bx);
    }

    void solveEquation(
            float *t1, float *t2,
            float a1, float a2,
            float a3, float a4,
            float b1, float b2) {
        /*
         * a1 a2 | b1
         * a3 a4 | b2
         *
         * detA  = (a1 * a4) - (a3 * a2)
         * detT1 = (b1 * a4) - (b2 * a2)
         * detT2 = (a1 * b2) - (a3 * b1)
         *
         * t1 = detT1 / detA
         * t2 = detT2 / detA
         */

        const float detA  = (a1 * a4) - (a3 * a2);
        const float detT1 = (b1 * a4) - (b2 * a2);
        const float detT2 = (a1 * b2) - (a3 * b1);

        if(fabs(detA) < 0.001f) {
            *t1 = -1.0f;
            *t2 = -1.0f;
        }

        const float invDetA = 1.0f / detA;

        *t1 = detT1 * invDetA;
        *t2 = detT2 * invDetA;
    }

    void calc2dIntersection(
            float *t1, float *t2,
            float ax, float ay,
            float bx, float by,
            float cx, float cy,
            float dx, float dy) {
        /* 
         * 1) Write intersection as interpolation:
         *      i = a + t1 * (b - a)
         *      i = c + t2 * (d - c)
         *
         * 2) 
         *      a + t1 * (b - a) = c + t2 * (d - c)
         *
         * 3) Rewrite equation between vectors:
         *      t1 * (b - a) + t2 * (c - d) = (c - a)
         *
         * 4) 
         *      Expand vectors to coords,
         *      write system of lin. equations, 
         *      solve for t1, t2:
         *
         *      t1 * (bx - ax) + t2 * (cx - dx) = (cx - ax)
         *      t1 * (by - ay) + t2 * (cy - dy) = (cy - ay)
         *
         */
        const float a1 = (bx - ax);
        const float a2 = (cx - dx);
        const float a3 = (by - ay);
        const float a4 = (cy - dy);

        const float b1 = (cx - ax);
        const float b2 = (cy - ay);

        solveEquation(t1, t2, a1, a2, a3, a4, b1, b2);
    }

    static void getBarycentricCoords(
            const float x, const float y,
            const float ax, const float ay,
            const float bx, const float by,
            const float cx, const float cy,
            float* v, float* w) {
        const float a1 = (bx - ax);
        const float a2 = (cx - ax);
        const float a3 = (by - ay);
        const float a4 = (cy - ay);

        const float b1 = (x - ax);
        const float b2 = (y - ay);

        solveEquation(v, w, a1, a2, a3, a4, b1, b2);
    }

    bool isInsideTriangle(
            const float x, const float y,
            const float ax, const float ay,
            const float bx, const float by,
            const float cx, const float cy) {

        float u, v;
        getBarycentricCoords(x, y, ax, ay, bx, by, cx, cy, &u, &v);

        return (u >= 0.0f && u <= 1.0f) && (v >= 0.0f && v <= 1.0f);
    }

    bool isInsideQuad(
            const float x, const float y,
            const float ax, const float ay,
            const float bx, const float by,
            const float cx, const float cy,
            const float dx, const float dy) {
        return
            isInsideTriangle(x, y, ax, ay, bx, by, cx, cy) || 
            isInsideTriangle(x, y, ax, ay, cx, cy, dx, dy);
    }

    typedef enum {
        POS_INSIDE  = 0x00,
        POS_LEFT    = 0x01,
        POS_RIGHT   = 0x02,
        POS_TOP     = 0x04,
        POS_BOTTOM  = 0x08
    } pos_t;

    pos_t getPosInRect(
            const float x, const float y, 
            const float xmin, const float ymin, 
            const float xmax, const float ymax) {
        int pos = POS_INSIDE;

        if(x < xmin) {
            pos |= POS_LEFT;
        } else if(x > xmax) {
            pos |= POS_RIGHT;
        }

        if(y < ymin) {
            pos |= POS_BOTTOM;
        } else if(y > ymax) {
            pos |= POS_TOP;
        }

        return (pos_t)pos;
    }

    /* 
     * Perform Cohen-Sutherland clipping of segment.
     *
     * @param segment (x1, y1), (x2, y2)
     * @param rectangle (xmin, ymin, xmax, ymax)
     *
     * @return 1 if segment lies inside, otherwise 0.
     */
    bool clipSegmentToRectangle(
            float *x1, float *y1,
            float *x2, float *y2,
            float xmin, float ymin, 
            float xmax, float ymax) {

        float ax = *x1;
        float ay = *y1;
        float bx = *x2;
        float by = *y2;

        /* Find position inside rectangle: */
        auto aPos = getPosInRect(ax, ay, xmin, ymin, xmax, ymax);
        auto bPos = getPosInRect(bx, by, xmin, ymin, xmax, ymax);

        while(1) {
            if((aPos == POS_INSIDE) && (bPos == POS_INSIDE)) {

                /* A and B are inside. */
                return true;
            } else if(aPos & bPos) {

                /* A and B are behind rectangle. */
                return false;
            } else {
                /* Get pos of any outter point: */
                const auto pos = (aPos == POS_INSIDE) ? bPos : aPos;

                float x = 0.0f;
                float y = 0.0f;

                /* If point lies in specific zone, calculate intersection: */
                if(pos & POS_BOTTOM) {
                    x = ax + ((ymin - ay) * (bx - ax)) / (by - ay);
                    y = ymin;
                } else if(pos & POS_TOP) {
                    x = ax + ((ymax - ay) * (bx - ax)) / (by - ay);
                    y = ymax;
                } else if(pos & POS_RIGHT) {
                    x = xmax;
                    y = ay + ((xmax - ax) * (by - ay)) / (bx - ax);
                } else if(pos & POS_LEFT) {
                    x = xmin;
                    y = ay + ((xmin - ax) * (by - ay)) / (bx - ax);
                }

                /* Refresh clipped point position: */
                if(pos == aPos) {
                    *x1 = x;
                    *y1 = y;
                     ax = x;
                     ay = y;
                    aPos = getPosInRect(ax, ay, xmin, ymin, xmax, ymax);
                } else if(pos == bPos) {
                    *x2 = x;
                    *y2 = y;
                     bx = x;
                     by = y;
                    bPos = getPosInRect(bx, by, xmin, ymin, xmax, ymax);
                }
            }
            /* Repeat as long, as points will be entirely inside rectangle. */
        }

        return true;
    }

    float getLength(const float ax, const float ay, const float bx, const float by) {
        const auto dx = (bx - ax);
        const auto dy = (by - ay);

        return sqrtf(dx * dx + dy * dy);
    }

    bool overlap(const float a0, const float a1, const float b0, const float b1) {
        return (min(a0, a1) <= max(b0, b1) && min(b0, b1) <= max(a0, a1));
    }

    bool checkBoxIntersection(
            const float ax, const float ay,
            const float bx, const float by,
            const float cx, const float cy,
            const float dx, const float dy) {
        return overlap(ax, bx, cx, dx) && overlap(ay, by, cy, dy);
    }
}
