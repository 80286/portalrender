#ifndef MAP_RENDER_H
#define MAP_RENDER_H

#include "Context.h"

struct SectorRange {
    SectorRange(const int sectorId, const int sx1, const int sx2) {
        m_sectorId = sectorId;
        m_sx1 = sx1;
        m_sx2 = sx2;
    }

    int m_sectorId;

    /* X range on screen: */
    int m_sx1;
    int m_sx2;
};

namespace MapView3d {
    Map::Vertex transformVertexToPlayerSpace(const PlayerObj*, const Map::Vertex*);
    void draw(Context*);
}

#endif
