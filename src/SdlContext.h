#ifndef SDLCONTEXT_H
#define SDLCONTEXT_H

#include "SDL2/SDL.h"
#include "EventHandler.h"

struct SdlContextScreen {
    int w;
    int h;
    uint32_t* data;
    int bytesPerPixel;
    int pitch;
};
    
class SdlContext {
public:
    SdlContext();

    void init();
    void destroy();
    bool isValid() const;
    void render();
    void handleKeyboardEvent();
    void setEventHandler(EventHandler* handler);

    SDL_Texture* m_tex;
    SDL_Window* m_window;
    SDL_Renderer* m_renderer;
    SdlContextScreen m_screen;
    EventHandler* m_eventHandler;
};

#endif
