#include "Context.h"
#include <math.h>
#include "DebugTools.h"
#include "MathTools.h"
#include "MapView2d.h"
#include "MapView3d.h"

void Context::init(Map::Map* map, uint32_t* data, const int screenWidth, const int screenHeight) {
    m_img.set(data, screenWidth, screenHeight);

    const float aspect = ((float)screenWidth / (float)screenHeight);
    const float fovY = 0.5f * M_PI;

    m_stop = 0;
    m_clippingEnabled = true;
    m_drawYMask = false;

    m_yLen = screenWidth;
    m_yTop = new int[m_yLen];
    m_yBottom = new int[m_yLen];

    printf("context_init(); fov: %g deg; aspect: %g\n", (fovY / M_PI) * 180.0f, aspect);

    m_frustum.far = 1024.0f;
    m_frustum.near = 1.0f;
    m_frustum.top = m_frustum.near * tanf(0.5f * fovY);
    m_frustum.right = m_frustum.top * aspect;

    m_map = map;
    m_mode = MODE_2D;

    m_player.m_x = map->m_player.pos.x;
    m_player.m_y = map->m_player.pos.y;
    m_player.m_z = map->m_player.z;
    m_player.m_sectorId = map->m_player.sector;
    m_player.rotate(map->m_player.angle);

    m_currentEdge = nullptr;
    m_currentSector = nullptr;

    m_resList.add("../res/tex0.png");
}

void Context::clearSurface() {
    m_img.clear();
}

void Context::initYArrays() {
    m_yMin = 0;
    m_yMax = m_img.m_height - 1;

    for(int x = 0; x < m_yLen; x++) {
        m_yTop[x] = m_yMin;
        m_yBottom[x] = m_yMax;
    }
}

void Context::shrinkYTop(const int x, const int value) {
    if(x < 0 || x >= m_yLen) {
        return;
    }

    const auto prev = m_yTop[x];
    m_yTop[x] = clamp(value, m_yTop[x], m_yMax);
}

void Context::shrinkYBottom(const int x, const int value) {
    if(x < 0 || x >= m_yLen) {
        return;
    }

    m_yBottom[x] = clamp(value, m_yMin, m_yBottom[x]);
}

void Context::print() {
    printf("context_print(); [initialSector: %d; x=%g, y=%g, z=%g, ang=%g]\n", 
            m_player.m_sectorId,
            m_player.m_x, 
            m_player.m_y, 
            m_player.m_z, 
            m_player.m_angle);

    const float leftPlaneAngle = ((atan2f(m_frustum.near, -m_frustum.right)) / M_PI) * 180.0f;
    const float rightPlaneAngle = ((atan2f(m_frustum.near, m_frustum.right)) / M_PI) * 180.0f;

    printf("Planes: near: %g; far: %g; right: %g; (leftAng, rightAng): (%g, %g)\n",
            m_frustum.near, m_frustum.far, m_frustum.right,
            leftPlaneAngle, rightPlaneAngle);
    printf("==========================================================\n");
}

void Context::free() {
    if(m_yTop) {
        delete [] m_yTop;
        m_yTop = nullptr;
    }

    if(m_yBottom) {
        delete [] m_yBottom;
        m_yBottom = nullptr;
    }

    m_resList.free();
}

void Context::drawYMask() {
    const auto width = m_img.m_width;
    const uint32_t color = 0x0000ff;

    const auto yMin = m_yMin;
    const auto yMax = m_yMax;

    for(int x = 0; x < width; ++x) {
        const auto yTop = m_yTop[x];
        const auto yBottom = m_yBottom[x];

        if(yMin != yTop) {
            m_img.drawLine(x, yMin, x, yTop, color);
        }

        if(yBottom != yMax) {
            m_img.drawLine(x, yBottom, x, yMax, color);
        }
    }
}

void Context::handleExit() {
    m_stop = 1;
}

void Context::handleKeyUp(const char inputSymbol) {
    int sector;
    
    auto player = &(m_player);
    const float dx = 5.0f * player->m_cosv;
    const float dy = 5.0f * player->m_sinv;

    switch(inputSymbol) {
    case 'w':
        player->move(m_map, dx, dy);
        break;
    case 's':
        player->move(m_map, -dx, -dy);
        break;
    case 'a':
        player->rotate(player->m_angle - 0.1f);
        break;
    case 'd':
        player->rotate(player->m_angle + 0.1f);
        break;
    case 'c':
        m_clippingEnabled = !m_clippingEnabled;
        r_printf("Clipping: [%s]\n", m_clippingEnabled ? "On" : "Off");
        break;
    case 'n':
        sector = m_map->m_player.sector;
        player->m_sectorId = (sector < m_map->m_sectors.size() - 1) ? sector + 1 : 0;
        break;
    case 'y':
        m_drawYMask = !m_drawYMask;
        break;
    case 'q':
        m_stop = 1;
        break;
    case 'm':
        m_mode = (m_mode == MODE_3D) ? MODE_2D : MODE_3D;
        break;
    default:
        break;
    }
    
    print();
}

void Context::draw() {
    auto& draw = (m_mode == MODE_2D) ? MapView2d::draw : MapView3d::draw;

    m_img.clear();
    draw(this);
}
