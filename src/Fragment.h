#ifndef FRAGMENT_H
#define FRAGMENT_H

#include <stdint.h>
#include <SDL2/SDL.h>

enum FragmentType {
    FT_MIDDLE = 0,
    FT_UPPER = 1,
    FT_LOWER = 2,
};

struct Fragment {
    float h1, h2;
    float y1, dy1;
    float y2, dy2;
    float x, dx;
    int32_t color;
    FragmentType type;

    int ay1, ay2, by1, by2;

    SDL_Surface *texture;

    int isHighlighted;
    bool initialized;

    Fragment();
    Fragment(const float lowerHeight, const float upperHeight, const FragmentType type);

    void clampY(const int* yTop, const int* yBottom, const int x);
    Fragment* set(const float lowerHeight, const float upperHeight, const FragmentType type);
};

#endif
