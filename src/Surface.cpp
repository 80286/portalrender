#include "Surface.h"

Surface::Surface() {
    m_upper = nullptr;
    m_middle = nullptr;
    m_lower = nullptr;
}

void Surface::print(int index) {
    char type[4];

    type[0] = (m_middle) ? 'M' : '-';
    type[1] = (m_upper) ? 'U' : '-';
    type[2] = (m_lower) ? 'L' : '-';
    type[3] = '\0';

    printf("[%3d] surface_print(); Type: %s; (sx1, sx2) = (%3d, %3d)\n", 
            index, type, m_a.x, m_b.x);

    auto printFragment = [](const char* prefix, const Fragment* f) {
        if(!f) {
            return;
        }
        printf("%s h(%g, %g) y(%g, %g)\n", prefix, f->h1, f->h2, f->y1, f->y2);
    };

    printFragment("Lower", m_lower);
    printFragment("Upper", m_upper);
    printFragment("Middle", m_middle);
}

void Surface::calcScreenYCords(Fragment* fragment, const Context* context, int& ay1, int& ay2, int& by1, int& by2) {
    /* 
     * Notice: y1 is related to height2, y2 to height1
     * (Screen y-coord is inverted) 
     * y = 0 -------
     *          B
     * A     __by1 (upper: h2)
     * ay1__/   |
     *  |       |
     * ay2___   |
     *       \_by2 (lower: h1)
     * y = H -------
     * 
     * ay1 --> ( fragment->y1 ) --> by1
     * ay2 --> ( fragment->y2 ) --> by2
     *
     *************************************************** */

    /* Transform heights to player space: */

    /* Lower height: */
    const float th1 = fragment->h1 - context->m_player.m_z;
    /* Upper height: */
    const float th2 = fragment->h2 - context->m_player.m_z;

    const auto screenHeight = context->m_img.m_height;

    /* Calc y for upper edge of wall: */
    ay1 = context->m_frustum.izToY(th2, m_a.iz, screenHeight);
    by1 = context->m_frustum.izToY(th2, m_b.iz, screenHeight);

    /* Calc y for lower edge of wall: */
    ay2 = context->m_frustum.izToY(th1, m_a.iz, screenHeight);
    by2 = context->m_frustum.izToY(th1, m_b.iz, screenHeight);
}

void Surface::calcYForFragments(const Context* context) {
    auto calc = [this, context](Fragment* f) {
        if(!f) {
            return;
        }

        calcScreenYCords(f, context, f->ay1, f->ay2, f->by1, f->by2);
    };

    calc(m_upper);
    calc(m_lower);
    calc(m_middle);
}

void Surface::calcY(Fragment* fragment, const Context* context, const int sx) {
    auto& ay1 = fragment->ay1;
    auto& ay2 = fragment->ay2;
    auto& by1 = fragment->by1;
    auto& by2 = fragment->by2;

    const float t = (float)(sx - m_a.x) / (float)(m_b.x - m_a.x);

    fragment->y1 = (float)ay1 + t * (float)(by1 - ay1);
    fragment->y2 = (float)ay2 + t * (float)(by2 - ay2);
    
    fragment->clampY(context->m_yTop, context->m_yBottom, sx);
}

void Surface::initFragmentCache(Fragment* fragment, const Context* context, const FragmentType fragmentType) {
    int ay1, ay2, by1, by2;
    calcScreenYCords(fragment, context, ay1, ay2, by1, by2);

    /* Inverted x-span of unclipped segment: */
    const float invXSpan = 1.0f / m_xSpan;

    /* Calc delta factors for y1, y2: */
    fragment->dy1 = (float)(by1 - ay1) * invXSpan;
    fragment->dy2 = (float)(by2 - ay2) * invXSpan;

    fragment->y1 = (float)ay1;
    fragment->y2 = (float)ay2;

    fragment->dx = m_length * invXSpan;

    /* Move current x to clipped part: */
    fragment->x = m_clipFactor * m_length;

    fragment->texture = context->m_resList.m_resources[0].m_surface;
}

void Surface::updateYValues(const Context* context, int sx) {
    auto calc = [this, context, &sx](Fragment* f) {
        if(!f) {
            return;
        }

        calcY(f, context, sx);
    };

    calc(m_lower);
    calc(m_upper);
    calc(m_middle);
}

void Surface::getA(const Fragment* fragment, int& x, int& y) const {
    x = m_a.x;
    y = fragment->ay1;
}

void Surface::getB(const Fragment* fragment, int& x, int& y) const {
    x = m_b.x;
    y = fragment->by1;
}

void Surface::getC(const Fragment* fragment, int& x, int& y) const {
    x = m_a.x;
    y = fragment->ay2;
}

void Surface::getD(const Fragment* fragment, int& x, int& y) const {
    x = m_b.x;
    y = fragment->by2;
}
