#ifndef MAPPARSER
#define MAPPARSER

#define BUFFER_SIZE 1024

#include <cstdio>
#include "Map.h"

namespace MapParser {
    enum Token {
        T_UNDEFINED = -1,
        T_SECTION_VERTICES, 
        T_SECTION_SECTORS,
        T_SECTION_PLAYER,
        T_SSECTION_EDGES,
        T_SSECTION_BEGIN,
        T_SSECTION_CLOSE,
    };

    enum SectionType { 
        ST_UNDEFINED = -1, 
        ST_VERTICES, 
        ST_SECTORS,
        ST_PLAYER,
    };

    enum SectorSection {
        SS_UNDEFINED = -1,
        SS_DESCRIPTION,
        SS_EDGES,
    };

    struct TokenString {
        Token token;
        const char* str;
    };

    class State {
    public:
        State();

        static Map::Map read(const char* filename);

        SectorSection m_currentSectorSection;
        SectionType m_currentSection;
        Token m_currentToken;
        const char* m_filename;

        char m_buffer[BUFFER_SIZE];
        char m_section[32];
        char* m_line;

        bool m_stop;
        int m_lineNum;
        FILE* m_f;

        Map::Map m_map;

    private:
        bool init(const char* filename);
        void initSectionStart();
        void parseLine(const char* line);
        void parseVertexLine();
        void parseSectorDescription();
        void parseSectorEdge();
        void parseSectorLine();
        void parsePlayerLine();

        void printMsg(const char* format, ...);
        void printMsgDebug(const char *format, ...);
        void printMsgUnexpected(const char *prefix);
    };

    Map::Map parse(const char*);
}

#endif
