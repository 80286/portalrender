#ifndef EVENTHANDLER_H
#define EVENTHANDLER_H

class EventHandler {
public:
    virtual void handleKeyUp(const char inputSymbol) {}
    virtual void handleExit() {}
};

#endif
