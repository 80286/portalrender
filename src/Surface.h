#ifndef SURFACE_T
#define SURFACE_T

#include "Context.h"
#include "Fragment.h"

struct ScreenVertex {
    ScreenVertex() {
        x = 0;
        xClamped = 0;
        iz = 0.0f;
    }

    int x;
    int xClamped;
    float iz;
};

class Surface {
public:
    Surface();

    void init();
    void print(int index);
    void calcScreenYCords(Fragment* fragment, const Context* context, int& ay1, int& ay2, int& by1, int& by2);
    void calcY(Fragment* fragment, const Context* context, const int sx);
    void calcYForFragments(const Context* context);
    void updateYValues(const Context* context, int sx);

    void getA(const Fragment* fragment, int& x, int& y) const;
    void getB(const Fragment* fragment, int& x, int& y) const;
    void getC(const Fragment* fragment, int& x, int& y) const;
    void getD(const Fragment* fragment, int& x, int& y) const;

    Map::Sector* m_sector;
    ScreenVertex m_a;
    ScreenVertex m_b;
    Map::Vertex* m_va;
    Map::Vertex* m_vb;

    float m_diz;
    float m_xSpan;
    float m_length;
    float m_clipFactor;
    float m_iz;

    int m_edgeColor;

    Fragment* m_upper;
    Fragment* m_middle;
    Fragment* m_lower;

    int m_isCacheInitialized;

private:
    void initFragmentCache(Fragment* fragment, const Context* context, const FragmentType fragmentType);
};

#endif
