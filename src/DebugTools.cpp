#include "DebugTools.h"

void r_printf(const char *format, ...) {
#ifdef MAP_RENDER_DEBUG
    va_list args;

    va_start(args, format);
    vprintf(format, args);
    va_end(args);
#endif
}
