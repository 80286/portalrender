#ifndef RENDERTOOLS_H
#define RENDERTOOLS_H

#include "Context.h"

namespace RenderTools
{
    bool clipSegment(const Context* context, Map::Vertex* a, Map::Vertex* b);
}

#endif
