#ifndef MAP_H
#define MAP_H

#include <vector>

namespace Map {
    struct Vertex {
        float x;
        float y;
    };

    struct Edge {
        /* Indices in vertices array: */
        int vertexA;
        int vertexB;

        /* Index in sectors array: */ 
        int neighbor;

        bool isPortal() const { return neighbor >= 0; }
    };

    struct Sector {
        float floorHeight;
        float ceilHeight;
        std::vector<Edge> edges;
    };

    struct MapObj {
        Vertex pos;
        float angle;
        float z;
        int sector;
    };

    class Map {
    public:
        Map();

        void print(const char*) const;

        void appendVertex(const Vertex *);
        void appendEmptySector();
        void appendEdgeToCurrentSector(Edge *);

        Sector* getCurrentSector();

        std::vector<Vertex> m_vertices;
        std::vector<Sector> m_sectors;
        MapObj m_player;
    };
}

#endif
