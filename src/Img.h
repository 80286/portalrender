#ifndef IMG_H
#define IMG_H

#include <cstdint>

class Img {
public:
    void set(uint32_t* data, const int w, const int h);

    uint32_t *getPixel(const int x, const int y);
    void clear();
    void drawPixel(const int x, const int y, const uint32_t color);
    void drawLine(int x1, int y1, int x2, int y2, uint32_t color);
    void drawPoint(int x, int y, uint32_t color);
    bool isValid(const int x, const int y) const;
    void drawQuad(
            const int& ax, const int& ay,
            const int& bx, const int& by,
            const int& cx, const int& cy,
            const int& dx, const int& dy, const uint32_t color);

    uint32_t* m_data;
    int m_width;
    int m_height;
    int m_bytesPerPixel;
};

#endif
