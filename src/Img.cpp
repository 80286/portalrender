#include "Img.h"

#include <math.h>
#include <stdio.h>
#include <string.h>

#include "MathTools.h"

void Img::set(uint32_t* data, const int w, const int h) {
    m_data = data;
    m_width = w;
    m_height = h;
    m_bytesPerPixel = 4;
}

bool Img::isValid(const int x, const int y) const {
    return (x >= 0) && (x < m_width) && (y >= 0) && (y < m_height);
}

uint32_t* Img::getPixel(const int x, const int y) {
    const auto offset = (m_width * y + x);
    return (uint32_t *)(m_data + offset);
}

void Img::drawPixel(const int x, const int y, const uint32_t color) {
    uint32_t* pixel = (uint32_t *)getPixel(x, y);

    *pixel = color;
}

void Img::clear() {
    memset(m_data, (uint32_t)0, m_height * m_width * m_bytesPerPixel);
}

void Img::drawLine(int x1, int y1, int x2, int y2, uint32_t color) {
	const float a = (float)(y2 - y1) / (float)(x2 - x1);
	const float b = (float)y2 - (a * (float)x2);

	if(fabs(a) >= 1.0) {
		const float m1 = min(y1, y2);
		const float m2 = max(y1, y2);

		for(float y = m1; y <= m2; y += 1.0f) {
			float x = (x2 - x1) != 0 ? ((y - b) / a) : x1;

            if(isValid(x, y)) {
                drawPixel(x, y, color);
            }
		}
	} else {
		const float m1 = min(x1, x2);
		const float m2 = max(x1, x2);

		for(float x = m1; x <= m2; x += 1.0f) {
            const auto y = a * x + b;

            if(isValid(x, y)) {
			    drawPixel(x, y, color);
            }
		}
	}
}
void Img::drawPoint(int x, int y, uint32_t color) {
    for(int j =  2; j > -2; j--) {
        for(int i = -2; i < 3; i++) {
            const int ax = x + i;
            int ay = y + j;

            if(isValid(ax, ay)) {
                drawPixel(ax, ay, color);
            }

            ay = y - j;
            if(ay >= 0 && ay < m_height) {
                drawPixel(ax, ay, color);
            }
        }
    }
}

void Img::drawQuad(
        const int& ax, const int& ay,
        const int& bx, const int& by,
        const int& cx, const int& cy,
        const int& dx, const int& dy, const uint32_t color) {

    /* AB */
    drawLine(ax, ay, bx, by, color);

    /* CD */
    drawLine(cx, cy, dx, dy, color);

    /* AC */
    drawLine(ax, ay, cx, cy, color);

    /* BD */
    drawLine(bx, by, dx, dy, color);
}
