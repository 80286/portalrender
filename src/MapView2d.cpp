#include "MapView2d.h"
#include "MapView3d.h"
#include "RenderTools.h"
#include "MathTools.h"
#include <math.h>

static float map_scale = 1.0f;

namespace MapView2d
{
    static bool clipSegmentToScreen(int screenWidth, int screenHeight, float* x1, float* y1, float* x2, float* y2) {
        Map::Vertex a, b;

        a.x = (float)*x1;
        a.y = (float)*y1;
        b.x = (float)*x2;
        b.y = (float)*y2;

        const float sw = ((float)screenWidth - 1.0f);
        const float sh = ((float)screenHeight - 1.0f);

        if(!MathTools::clipSegmentToRectangle(
                &(a.x), &(a.y), 
                &(b.x), &(b.y),
                0.0f, 0.0f, sw, sh)) {
            return false;
        }

        *x1 = a.x;
        *y1 = a.y;
        *x2 = b.x;
        *y2 = b.y;

        return true;
    }

    static Map::Vertex transform(const float ix, const float iy, const Map::Vertex* v) {
        Map::Vertex result;

        result.x = ix + (map_scale * v->x);
        result.y = iy - (map_scale * v->y);

        return result;
    }

    static void drawSegment(Img& img, 
            const Map::Vertex* a,
            const Map::Vertex* b,
            int drawEndPoints, 
            int lineColor) {
        const int endPointColor = 0x00ff00;

        auto ca = *a;
        auto cb = *b;

        if(!clipSegmentToScreen(img.m_width, img.m_height, &(ca.x), &(ca.y), &(cb.x), &(cb.y))) {
            return;
        }

        if(drawEndPoints) {
            img.drawPoint(ca.x, ca.y, endPointColor);
            img.drawPoint(cb.x, cb.y, endPointColor);
        }

        img.drawLine(ca.x, ca.y, cb.x, cb.y, lineColor);
    }

    static void drawFrustum(Context* context, int sx, int sy) {
        Map::Vertex A, B, C, D;

        const int xAxisColor = 0x0000AA;
        const int frustumColor = 0x000088;

        auto& img = context->m_img;

        const auto f = context->m_frustum.far;
        const auto n = context->m_frustum.near;
        const auto r = context->m_frustum.right;

        /* Draw x=0 axis: */
        A.x = 0;
        A.y = sy;
        B.x = img.m_width - 1;
        B.y = sy;
        drawSegment(img, &A, &B, 0, xAxisColor);

        /* Find view frustum XZ points: */
        const float halfOfFrustumXWidth = (f / n) * r;
        const float nearPlaneY = (float)sy - map_scale * n;
        const float farPlaneY  = (float)sy - map_scale * f;

        const float nearAx = (float)sx - map_scale * r;
        const float nearBx = (float)sx + map_scale * r;

        const float farAx = (float)sx - map_scale * halfOfFrustumXWidth;
        const float farBx = (float)sx + map_scale * halfOfFrustumXWidth;

        /* 
         * A(nearAx, nearPlaneY)
         * B(nearBx, nearPlaneY)
         * C(farAx,  farPlaneY)
         * D(farBx,  farPlaneY)
         */
        A.x = nearAx;
        A.y = nearPlaneY;
        B.x = nearBx;
        B.y = nearPlaneY;
        C.x = farAx;
        C.y = farPlaneY;
        D.x = farBx;
        D.y = farPlaneY;

        /* Draw near plane AB: */
        drawSegment(img, &A, &B, 0, frustumColor);

        /* Draw far plane CD: */
        drawSegment(img, &C, &D, 0, frustumColor);

        /* Draw slopes AC, BD: */
        drawSegment(img, &A, &C, 0, frustumColor);
        drawSegment(img, &B, &D, 0, frustumColor);
    }

    static void drawEdge(Context* context, const Map::Edge* edge, float sx, float sy, int segmentColor) {
        const auto player = &(context->m_player);

        auto edgeVertexA = &context->m_map->m_vertices[edge->vertexA];
        auto edgeVertexB = &context->m_map->m_vertices[edge->vertexB];

        /* Unclipped vertices: */
        auto uc_va = MapView3d::transformVertexToPlayerSpace(player, edgeVertexA);
        auto uc_vb = MapView3d::transformVertexToPlayerSpace(player, edgeVertexB);

        /* Vertices to be clipped: */
        auto va = uc_va;
        auto vb = uc_vb;

        auto& img = context->m_img;

        /* Clip segment to viewing frustum: */
        const auto isInsideFrustum = context->m_clippingEnabled ? RenderTools::clipSegment(context, &va, &vb) : true;

        if(isInsideFrustum) {
            const auto ua = transform(sx, sy, &uc_va);
            const auto ub = transform(sx, sy, &uc_vb);

            const auto ca = transform(sx, sy, &va);
            const auto cb = transform(sx, sy, &vb);

            /* At first, draw complete unclipped segment: */
            drawSegment(img, &ua, &ub, 0, segmentColor);

            /* Then draw clipped: */
            drawSegment(img, &ca, &cb, 1, 0x00ff00);
        } else {
            const auto ua = transform(sx, sy, &uc_va);
            const auto ub = transform(sx, sy, &uc_vb);

            /* Draw unclipped segment: */
            drawSegment(img, &ua, &ub, 0, segmentColor);
        }
    }

    static void drawSector(Context* context, const Map::Sector* sector, float sx, float sy, int segmentColor) {
        for(const auto& edge: sector->edges) {
            drawEdge(context, &edge, sx, sy, segmentColor);
        }
    }

    void draw(Context *context) {
        auto& map = context->m_map;

        /* Point (0, 0) of player space on screen: */
        const float sx = (float)context->m_img.m_width * 0.5f;
        /* const float sy = (float)context->m_sdlCtx->m_screen.h - 1.0f; */
        const float sy = (float)context->m_img.m_height * 0.75f;

        drawFrustum(context, sx, sy);

        for(const auto& sector: map->m_sectors) {
            drawSector(context, &sector, sx, sy, 0x808080);
        }
    }
}
