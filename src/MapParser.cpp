#include "MapParser.h"
#include <string.h>
#include <ctype.h>
#include <cstdarg>

static const char LINE_COMMENT_SYM = '#';

static int MAP_DEBUG = 1;

static char *str_trim(char *str) {
    while(*str && isspace(*str)) {
        str++;
    }

    char* ptr = str + strlen(str) - 1;
    while(*ptr && isspace(*ptr)) {
        ptr--;
    }

    *(ptr + 1) = '\0';

    return str;
}

static int str_isEmpty(const char *str) {
    while(*str) {
        if(!isspace(*str)) {
            return 0;
        }
        str++;
    }

    return 1;
}

static void str_removeLineComment(char *str) {
    char* commentSym = strchr(str, LINE_COMMENT_SYM);
    
    if(commentSym) {
        *commentSym = '\0';
    }
}

namespace MapParser {
    static const TokenString tokenStrings[] = {
        {Token::T_SECTION_VERTICES, "vertices:"},
        {Token::T_SECTION_SECTORS, "sectors:"},
        {Token::T_SECTION_PLAYER, "player:"},
        {Token::T_SSECTION_EDGES, "edges:"},
        {Token::T_SSECTION_BEGIN, "{{{"},
        {Token::T_SSECTION_CLOSE, "}}}"},
        {Token::T_UNDEFINED, NULL},
    };

    static int isSectionName(Token token) {
        return (token == Token::T_SECTION_SECTORS
             || token == Token::T_SECTION_VERTICES
             || token == Token::T_SECTION_PLAYER);
    }

    static Token mapParser_getToken(const char *str) {
        for(auto ptr = tokenStrings; ptr->token != Token::T_UNDEFINED; ptr++) {
            if(strcmp(str, ptr->str) == 0) {
                return ptr->token;
            }
        }

        return Token::T_UNDEFINED;
    }

    void State::printMsg(const char* format, ...) {
        va_list args;

        va_start(args, format);
        fprintf(stderr, "%s: %d: ", m_filename, m_lineNum);
        vfprintf(stderr, format, args);
        va_end(args);
    }

    void State::printMsgDebug(const char *format, ...) {
        va_list args;

        if(MAP_DEBUG == 0) {
            return;
        }

        va_start(args, format);
        printf("Debug: ");
        vprintf(format, args);
        va_end(args);
    }

    void State::printMsgUnexpected(const char *prefix) {
        printMsgDebug("SectionState: %d; SSectionState: %d\n", m_currentSection, m_currentSectorSection);
        printMsg("%s '%s'.\n", prefix, m_line);

        m_stop = true;
    }

    static SectionType TokenToSectionType(Token token) {
        switch(token) {
        case Token::T_SECTION_SECTORS:
            return SectionType::ST_SECTORS;
            break;
        case Token::T_SECTION_VERTICES:
            return SectionType::ST_VERTICES;
            break;
        case Token::T_SECTION_PLAYER:
            return SectionType::ST_PLAYER;
        default:
            return SectionType::ST_UNDEFINED;
            break;
        }
    }

    State::State() {
        m_f = nullptr;
        m_filename = nullptr;
        m_currentSection = ST_UNDEFINED;
        m_currentSectorSection = SS_UNDEFINED;
        m_currentToken = T_UNDEFINED;
        m_stop = false;
        m_lineNum = 0;
        m_line = nullptr;
    }

    void State::initSectionStart() {
        strncpy(m_section, m_line, 32);

        if(m_currentToken == Token::T_UNDEFINED) {
            return;
        }

        m_currentSection = TokenToSectionType(m_currentToken);

        printMsgDebug("Found section: '%s' (%d)\n", 
                m_section, 
                m_currentSection);

        /* Initialize current sector section: */
        if(m_currentToken == Token::T_SECTION_SECTORS) {
            m_currentSectorSection = SectorSection::SS_UNDEFINED;
        }
    }

    bool State::init(const char *filename) {
        auto f = fopen(filename, "r");

        if(!f) {
            fprintf(stderr, "Unable to open file '%s'.\n", filename);
            perror("fopen");
            return false;
        }

        m_f = f;
        m_filename = filename;

        return true;
    }

    void State::parseVertexLine() {
        Map::Vertex vertex;

        const int matched = sscanf(m_line, "%f %f", &(vertex.x), &(vertex.y));

        if(matched == 2) {
            printMsgDebug("Vertex: (%g, %g);\n", vertex.x, vertex.y);
            m_map.appendVertex(&vertex);
        } else {
            printMsgUnexpected("Unexpected");
        }
    }

    void State::parseSectorDescription() {
        char property[32];
        int numOfBytes;
        float value;

        printMsgDebug("Sector[%d]: mapParser_parseSectorDescription(); line='%s'\n", 
                m_map.m_sectors.size() - 1,
                m_line);

        sscanf(m_line, "%32s%n", property, &numOfBytes);
        const int matched = sscanf(m_line + numOfBytes, "%f", &value);

        if(matched != 1) {
            printMsgUnexpected("Expected float value, found");
            return;
        }

        Map::Sector* sector = m_map.getCurrentSector();

        if(strcmp(property, "floorHeight") == 0) {
            sector->floorHeight = value;
        } else if(strcmp(property, "ceilHeight") == 0) {
            sector->ceilHeight = value;
        } else {
            printMsgUnexpected("Unexpected sector property");
        }
    }

    void State::parseSectorEdge() {
        printMsgDebug("Sector[%d]: mapParser_parseSectorEdge(); line='%s'\n", 
                m_map.m_sectors.size() - 1,
                m_line);

        Map::Edge edge;
        const int matched = sscanf(m_line, "%d %d %d", 
                &(edge.vertexA),
                &(edge.vertexB),
                &(edge.neighbor));

        if(matched == 3) {
            m_map.appendEdgeToCurrentSector(&edge);
        } else {
            printMsgUnexpected("Unexpected");
        }
    }

    void State::parseSectorLine() {
        switch(m_currentToken) {
        case Token::T_SSECTION_BEGIN:
            m_currentSectorSection = SectorSection::SS_DESCRIPTION;
            m_map.appendEmptySector();
            return;
        case Token::T_SSECTION_CLOSE:
            if(m_currentSectorSection != SectorSection::SS_EDGES) {
                printMsgUnexpected("Unexpected");
                return;
            }
            m_currentSectorSection = SectorSection::SS_UNDEFINED;
            return;
        case Token::T_SSECTION_EDGES:
            m_currentSectorSection = SectorSection::SS_EDGES;
            return;
        default:
            break;
        }

        switch(m_currentSectorSection) {
        case SectorSection::SS_DESCRIPTION:
            parseSectorDescription();
            break;
        case SectorSection::SS_EDGES:
            parseSectorEdge();
            break;
        default:
        case SectorSection::SS_UNDEFINED:
            printMsgUnexpected("Expected '{{{', found ");
            break;
        }
    }

    void State::parsePlayerLine() {
        char property[32];
        int numOfBytes;

        sscanf(m_line, "%32s%n", property, &numOfBytes);

        if(strcmp(property, "pos") == 0) {
            Map::Vertex* pos = &(m_map.m_player.pos);
            sscanf(m_line + numOfBytes, 
                    "%f %f %f", 
                    &(pos->x),
                    &(pos->y),
                    &(m_map.m_player.z));
        } else if(strcmp(property, "angle") == 0) {
            sscanf(m_line + numOfBytes, 
                    "%f", 
                    &(m_map.m_player.angle));
        } else if (strcmp(property, "sector") == 0) {
            sscanf(m_line + numOfBytes, 
                    "%d", 
                    &(m_map.m_player.sector));
        } else {
            printMsgUnexpected("Unexpected player property");
        }
    }

    void State::parseLine(const char* line) {
        m_lineNum++;

        str_removeLineComment(m_buffer);

        m_line = str_trim(m_buffer);

        if(str_isEmpty(m_line)) {
            return;
        }

        m_currentToken = mapParser_getToken(m_line);

        if(isSectionName(m_currentToken)) {
            initSectionStart();
            printMsgDebug("Found section '%s'.\n", m_line);
            return;
        }

        switch(m_currentSection) {
        case SectionType::ST_VERTICES:
            parseVertexLine();
            break;
        case SectionType::ST_SECTORS:
            parseSectorLine();
            break;
        case SectionType::ST_PLAYER:
            parsePlayerLine();
            break;
        case SectionType::ST_UNDEFINED:
            printMsgUnexpected("Unexpected");
            break;
        }
    }

    Map::Map State::read(const char* filename) {
        State state;

        if(!state.init(filename)) {
            return state.m_map;
        }

        while(fgets(state.m_buffer, BUFFER_SIZE, state.m_f)) {
            state.parseLine(state.m_buffer);

            if(state.m_stop) {
                break;
            }
        }

        if(state.m_currentSectorSection != SectorSection::SS_UNDEFINED) {
            state.printMsg("Expected '}}}', reached end of file.\n");
        }

        fclose(state.m_f);

        return state.m_map;
    }

    Map::Map parse(const char* filename) {
        auto map = State::read(filename);

        map.print(filename);

        return map;
    }
}
