#ifndef MATH_TOOLS_H
#define MATH_TOOLS_H

template <class T>
T max(T a, T b) {
	return a > b ? a : b;
}

template <class T>
T min(T a, T b) {
	return a < b ? a : b;
}

template <class T>
T clamp(T x, T minval, T maxval) {
    if(x < minval) {
        x = minval;
    }

    if(x > maxval) {
        x = maxval;
    }

    return x;
}

namespace MathTools {
    int pointOnSide(
            float ax, float ay, 
            float bx, float by, 
            float cx, float cy);

    void calc2dIntersection(
            float *t1, float *t2,
            float ax, float ay,
            float bx, float by,
            float cx, float cy,
            float dx, float dy);

    bool clipSegmentToRectangle(
            float *x1, float *y1,
            float *x2, float *y2,
            float xmin, float ymin, 
            float xmax, float ymax);

    float getLength(const float ax, const float ay, const float bx, const float by);

    bool isInsideTriangle(
            const float x, const float y,
            const float ax, const float ay,
            const float bx, const float by,
            const float cx, const float cy);

    bool isInsideQuad(
            const float x, const float y,
            const float ax, const float ay,
            const float bx, const float by,
            const float cx, const float cy,
            const float dx, const float dy);

    bool overlap(const float a0, const float a1, const float b0, const float b1);
    bool checkBoxIntersection(
            const float ax, const float ay,
            const float bx, const float by,
            const float cx, const float cy,
            const float dx, const float dy);
}

#endif
