#include "RenderTools.h"
#include <cmath>

namespace RenderTools
{
    static float calcPlaneEquationForDiff(const Map::Vertex* a, const float pointOnPlane[2], const float normal[2]) {
        return (normal[0] * (a->x - pointOnPlane[0])) +
               (normal[1] * (a->y - pointOnPlane[1]));
    }

    static void interpVertex(Map::Vertex* result, const Map::Vertex* a, const Map::Vertex* b, const float t) {
        result->x = a->x + t * (b->x - a->x);
        result->y = a->y + t * (b->y - a->y);
    }

    bool clipSegmentToPlane(
            Map::Vertex* a, 
            Map::Vertex* b, 
            const float pointOnPlane[2],
            const float normal[2]) {
        /* int aInside, bInside; */
        /* float t, d1, d2; */

        /* 
         * A, B - vertices of clipped segment
         * P - point on plane
         * n - normal vector of plane
         *
         * 1. Find intersection:
         * I = A + t (B - A)
         *
         * 2. Subtract P:
         * (I - P) = (A - P) + t((A - P) - (B - P))
         *
         * 3. Insert points to plane equation:
         * dot(n, I - P) = dot(n, A - P) + t( dot(n, A - P) - dot(n, B - P))
         *
         * 4. I and P lies on plane, so lhs == 0
         * 0 = dot(n, A - P) + t (dot(n, A - P) - dot(n, B - P))
         *
         * 5. Simplify, then calc t:
         * 0 = d1 + t (d1 - d2)
         * t = -d1 / (d1 - d2) = d1 / (d2 - d1)
         *
         */

        /* Calc dot(normal, a - pointOnPlane): */
        const float d1 = calcPlaneEquationForDiff(a, pointOnPlane, normal);
        /* Calc dot(normal, b - pointOnPlane): */
        const float d2 = calcPlaneEquationForDiff(b, pointOnPlane, normal);

        const bool aInside = d1 > 0.0f;
        const bool bInside = d2 > 0.0f;

        if(aInside && bInside) {
            return true;
        }
        if(!aInside && !bInside) {
            return false;
        }

        const float t = d1 / (d1 - d2);

        if(!aInside && bInside) {
            /* a := a + t * (b - a); */
            interpVertex(a, a, b, t);
        } else if(aInside && !bInside) {
            /* b := a + t * (b - a); */
            interpVertex(b, a, b, t);
        }

        return true;
    }

    bool clipSegment(const Context* context, Map::Vertex* a, Map::Vertex* b) {
        const float l = -context->m_frustum.right;
        const float r =  context->m_frustum.right;
        const float f =  context->m_frustum.far;
        const float n =  context->m_frustum.near;

        /* Each plane normal vector is directed into interior of
         * viewing frustum: */
        struct plane {
            float pointOnPlane[2];
            float normal[2];
        } planes[] = {
            /* Near: */
            { {0, n}, {0.0f,  1.0f} },

            /* Far: */
            { {0, f}, {0.0f, -1.0f} },

            /* Left: */
            { {0.0f, 0.0f}, { n, -l} },

            /* Right: */
            { {0.0f, 0.0f}, {-n,  r} }
        };

        const int numOfPlanes = sizeof(planes) / sizeof(planes[0]);

        for(int i = 0; i < numOfPlanes; ++i) {
            const auto isEdgeBehindPlane = !clipSegmentToPlane(a, b, planes[i].pointOnPlane, planes[i].normal);

            if(isEdgeBehindPlane) {
                return false;
            }
        }

        return true;
    }
}
