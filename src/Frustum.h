#ifndef FRUSTUM_H
#define FRUSTUM_H

struct Frustum {
public:
    float far;
    float near;
    float right;
    float top;

    int izToX(const float x, const float iz, const int screenWidth) const;
    int izToY(const float height, const float iz, const int screenHeight) const;
};

#endif
