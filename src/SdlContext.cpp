#include "SdlContext.h"
#include <stdlib.h>

SdlContext::SdlContext()
    : m_window(nullptr)
    , m_renderer(nullptr)
    , m_tex(nullptr)
    , m_eventHandler(nullptr) {
}

void SdlContext::init() {
    m_screen.data = nullptr;
    m_screen.w = 1024;
    m_screen.h = 768;

    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0) {
        fprintf(stderr, "Unable to initialize SDL: %s\n", SDL_GetError());
        return;
    }

    if((m_window = SDL_CreateWindow("Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_screen.w, m_screen.h, 0)) == nullptr)
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        return;
    }
    
    if((m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_ACCELERATED)) == nullptr)
    {
        destroy();
        fprintf(stderr, "%s\n", SDL_GetError());
        return;
    }

    if((m_tex = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_STREAMING, m_screen.w, m_screen.h)) == nullptr)
    {
        destroy();
        fprintf(stderr, "%s\n", SDL_GetError());
        return;
    }

    m_screen.bytesPerPixel = sizeof(uint32_t);
    m_screen.pitch = (m_screen.w * m_screen.bytesPerPixel);
    m_screen.data = new uint32_t[m_screen.w * m_screen.h];
}

void SdlContext::render() {
    SDL_RenderClear(m_renderer);
    SDL_UpdateTexture(m_tex, nullptr, m_screen.data, m_screen.pitch);
    SDL_RenderCopy(m_renderer, m_tex, nullptr, nullptr);
    SDL_RenderPresent(m_renderer);
}

void SdlContext::destroy() {
    if(m_tex) {
        SDL_DestroyTexture(m_tex);
        m_tex = nullptr;
    }

    if(m_renderer) {
        SDL_DestroyRenderer(m_renderer);
        m_renderer = nullptr;
    }

    if(m_window) {
        SDL_DestroyWindow(m_window);
        m_window = nullptr;
    }

    if(m_screen.data) {
        delete [] m_screen.data;
    }

    m_screen.data = nullptr;
    SDL_Quit();
}

bool SdlContext::isValid() const {
    return m_window && m_renderer && m_screen.data;
}

void SdlContext::handleKeyboardEvent() {
    if(!m_eventHandler) {
        return;
    }

    SDL_Event event;
    char symbol;

    while(SDL_PollEvent(&event)) {
        switch(event.type) {
        case SDL_KEYDOWN:
            symbol = event.key.keysym.sym;
            m_eventHandler->handleKeyUp(symbol);
            break;
        case SDL_QUIT:
            m_eventHandler->handleExit();
            return;
        default:
            break;
        }
    }
}

void SdlContext::setEventHandler(EventHandler* handler) {
    m_eventHandler = handler;
}
