#include "PlayerObj.h"
#include "MathTools.h"
#include <cstdio>
#include <cmath>

static int getNextSectorId(const Map::Map* map, const PlayerObj* player, const int lastSectorId, const float dx, const float dy) {
    const auto& sectors = map->m_sectors;

    if(lastSectorId < 0 || lastSectorId > sectors.size()) {
        return -1;
    }

    const auto& px = player->m_x;
    const auto& py = player->m_y;

    const auto nx = px + dx;
    const auto ny = py + dy;

    const auto& initialSector = sectors[lastSectorId];

    for(const auto& edge: initialSector.edges) {
        const auto& va = map->m_vertices[edge.vertexA];
        const auto& vb = map->m_vertices[edge.vertexB];

        const auto isPlayerInsideSector = (MathTools::pointOnSide(va.x, va.y, vb.x, vb.y, nx, ny) < 0);
        const auto isPlayerTouchingEdge = MathTools::checkBoxIntersection(px, py, nx, ny, va.x, va.y, vb.x, vb.y);

        if(edge.isPortal() && isPlayerInsideSector && isPlayerTouchingEdge) {
            return edge.neighbor;
        }
    }

    return lastSectorId;
}

void PlayerObj::move(Map::Map* map, const float dx, const float dy) {
    const auto previousSectorId = m_sectorId;
    m_sectorId = getNextSectorId(map, this, previousSectorId, dx, dy);

    if(previousSectorId != m_sectorId) {
        printf("movePlayer: active sector id: %d -> %d\n", previousSectorId, m_sectorId);
    }

    if(m_sectorId >= 0) {
        m_x += dx;
        m_y += dy;
        m_z = map->m_player.z + map->m_sectors[m_sectorId].floorHeight;
    }
}

void PlayerObj::rotate(const float newAngle) {
    m_angle = newAngle;
    m_sinv = sinf(m_angle);
    m_cosv = cosf(m_angle);
}
