#include "Fragment.h"
#include "MathTools.h"

Fragment::Fragment(const float lowerHeight, const float upperHeight, const FragmentType fType) {
    set(lowerHeight, upperHeight, type);
}

Fragment::Fragment() {
    initialized = false;
}

Fragment* Fragment::set(const float lowerHeight, const float upperHeight, const FragmentType fType) {
    h1 = lowerHeight;
    h2 = upperHeight;
    type = fType;
    isHighlighted = 0;
    ay1 = 0;
    ay2 = 0;
    by1 = 0;
    by2 = 0;

    switch(type) {
        case FT_LOWER:
            color = 0xff0000;
            break;
        case FT_MIDDLE:
            color = 0x666666;
            break;
        case FT_UPPER:
            color = 0x00ff00;
            break;
    }

    initialized = true;

    return this;
}

void Fragment::clampY(const int* yTop, const int* yBottom, const int x) {
    const float top = (float)yTop[x];
    const float bottom = (float)yBottom[x];

    y1 = clamp(y1, top, bottom);
    y2 = clamp(y2, top, bottom);
}
