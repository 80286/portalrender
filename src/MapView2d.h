#ifndef MAPVIEW2D_H
#define MAPVIEW2D_H

#include "Context.h"

namespace MapView2d
{
    void draw(Context* context);
}

#endif
