#include "Map.h"
#include <stdio.h>
#include <stdlib.h>
#include <set>

namespace Map {
    Map::Map(void) {
        m_player.pos.x = 0.0f;
        m_player.pos.y = 0.0f;
        m_player.angle = 0.0f;
    }

    void Map::print(const char *mapFilename) const {
        printf("map_print();\n");
        printf("Map: %s; %d vertices, %d sectors\n", 
                mapFilename,
                (int)m_vertices.size(), 
                (int)m_sectors.size());
        printf("Player at point (%g, %g), angle: %g, sector: %d\n",
                m_player.pos.x,
                m_player.pos.y,
                m_player.angle,
                m_player.sector);

        int vertexIdx = 0;
        for(const auto& vertex: m_vertices) {
            printf("Vertex[%d] = (%g, %g);\n", vertexIdx++, vertex.x, vertex.y);
        }

        int sectorIdx = 0;
        for(const auto& sector: m_sectors) {
            printf("Sector[%d] floor: %g; ceil: %g; edges: %d\n", 
                    sectorIdx++, 
                    sector.floorHeight,
                    sector.ceilHeight,
                    (int)sector.edges.size());

            int edgeIdx = 0;
            for(const auto& edge: sector.edges) {
                printf("Edge[%d] (%d, %d) Neighbor: %d;\n", edgeIdx++, edge.vertexA, edge.vertexB, edge.neighbor);
            }
        }
    }

    Sector* Map::getCurrentSector() {
        return &m_sectors.back();
    }

    void Map::appendVertex(const Vertex *v) {
        m_vertices.push_back(*v);
    }

    void Map::appendEmptySector() {
        Sector sector;

        sector.floorHeight = 0.0f;
        sector.ceilHeight = 128.0f;

        m_sectors.push_back(sector);
    }

    void Map::appendEdgeToCurrentSector(Edge* edge) {
        auto currentSector = getCurrentSector();

        currentSector->edges.push_back(*edge);
    }
}
