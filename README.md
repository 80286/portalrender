Simple portal renderer 
=====================================================

![Screenshot](https://bitbucket.org/80286/portalrender/raw/master/screenshots/231121.gif)

C++ implementation of portal renderer based on [bisqwit portal renderer](https://www.youtube.com/watch?v=HQYsFshbkYw).
The main goal of this app was to translate mentioned C implementation into simple and self-describing C++ implementation.

This program:

- reads map in text format (check text files from `maps` directory) - map consists of list of sectors defined by list of edges
- shows 2d preview of loaded map, highlights all edges that were clipped into the viewing frustum (check `src/MapView2d.cpp`)
- draws non-textured 3d preview from any point of map (check `src/MapView3d.cpp`)

Caveats:

- program doesn't sort list of visible edges that are close to the viewer - this causes rendering issues on many maps (for example, load map `map/map1.txt`)
- there is still problem with infinite traversal of adjacent portal sectors, for now renderer simply limits number of allowed surfaces per sector (`src/MapView3d.cpp`: `renderedSectors`)

### How to build and launch

- `git clone https://bitbucket.org/80286/portalrender.git && cd portalrender`
- `mkdir build`
- `cd build && cmake ..`
- `make`
- `./portal`


### Map selection

- `./portal ../map/map1.txt`

### Keybindings
---------------
- `m` - toggle between 2d and 3d preview
- `w` `s` `a` `d` - navigation
- `y` - toggle rendering of y mask used for drawing upper/lower surfaces
- `c` - toggle clipping
